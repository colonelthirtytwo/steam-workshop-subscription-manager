
Ever wanted to sync your subscribed workshop items with those of your friends? Or maybe you want to try some new mods but don't want to abandon your old set?

This program lets you import and export lists of Steam workshop mods in bulk.
