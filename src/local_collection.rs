use std::hash::{
	Hash,
	Hasher,
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Item {
	pub published_file_id: u64,
	pub name: String,
}
impl PartialEq for Item {
	fn eq(&self, other: &Self) -> bool {
		self.published_file_id == other.published_file_id
	}
}
impl Eq for Item {}
impl Hash for Item {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.published_file_id.hash(state);
	}
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Collection {
	pub app_id: u32,
	pub items: Vec<Item>,
}
