use std::{
	marker::PhantomData,
	sync::{
		Arc,
		Mutex,
	},
};

use steamworks;

/// Loads all subscribed items for the current app, blocking until they are all loaded.
pub fn load_subscriptions(
	client: &steamworks::Client,
	single: &steamworks::SingleClient<steamworks::ClientManager>,
) -> Result<Vec<steamworks::QueryResult>, String> {
	let utils = client.utils();
	let ugc = client.ugc();
	let users = client.user();

	let app_id = utils.app_id();
	let own_steamid = users.steam_id();
	let account_id = own_steamid.account_id();

	let paginator = Paginator::new(|page| {
		ugc.query_user(
			account_id,
			steamworks::UserList::Subscribed,
			steamworks::UGCType::Items,
			steamworks::UserListOrder::SubscriptionDateDesc,
			steamworks::AppIDs::CreatorAppId(app_id),
			page,
		)
	});

	while !paginator.done() {
		single.run_callbacks();
		::std::thread::sleep(::std::time::Duration::from_millis(100));
	}

	paginator.result()
}

/// Subscribes to a workshop item, blocking until done
pub fn subscribe(
	client: &steamworks::Client,
	single: &steamworks::SingleClient<steamworks::ClientManager>,
	published_file_id: steamworks::PublishedFileId,
) -> Result<(), String> {
	let ugc = client.ugc();

	let state: Arc<Mutex<(bool, Result<(), String>)>> = Arc::new(Mutex::new((false, Ok(()))));

	let state2 = Arc::clone(&state);
	ugc.subscribe_item(published_file_id, move |res| {
		let mut lock = state2.lock().unwrap();
		*lock = (true, res.map_err(|e| format!("{}", e)));
	});

	loop {
		{
			if state.lock().unwrap().0 {
				break;
			}
		}
		single.run_callbacks();
		::std::thread::sleep(::std::time::Duration::from_millis(100));
	}

	Arc::try_unwrap(state).unwrap().into_inner().unwrap().1
}

/// Unsubscribes from a workshop item, blocking until done
pub fn unsubscribe(
	client: &steamworks::Client,
	single: &steamworks::SingleClient<steamworks::ClientManager>,
	published_file_id: steamworks::PublishedFileId,
) -> Result<(), String> {
	let ugc = client.ugc();

	let state: Arc<Mutex<(bool, Result<(), String>)>> = Arc::new(Mutex::new((false, Ok(()))));

	let state2 = Arc::clone(&state);
	ugc.unsubscribe_item(published_file_id, move |res| {
		let mut lock = state2.lock().unwrap();
		*lock = (true, res.map_err(|e| format!("{}", e)));
	});

	loop {
		{
			if state.lock().unwrap().0 {
				break;
			}
		}
		single.run_callbacks();
		::std::thread::sleep(::std::time::Duration::from_millis(100));
	}

	Arc::try_unwrap(state).unwrap().into_inner().unwrap().1
}

pub fn add_to_collection(
	client: &steamworks::Client,
	single: &steamworks::SingleClient<steamworks::ClientManager>,
	collection_id: steamworks::PublishedFileId,
	item_id: steamworks::PublishedFileId,
) -> Result<(), String> {
	let ugc = client.ugc();

	let state: Arc<Mutex<(bool, Result<(), String>)>> = Arc::new(Mutex::new((false, Ok(()))));

	let state2 = Arc::clone(&state);
	ugc.add_dependency(collection_id, item_id, move |res| {
		let mut lock = state2.lock().unwrap();
		*lock = (true, res.map_err(|e| format!("{}", e)));
	});

	loop {
		{
			if state.lock().unwrap().0 {
				break;
			}
		}
		single.run_callbacks();
		::std::thread::sleep(::std::time::Duration::from_millis(100));
	}

	Arc::try_unwrap(state).unwrap().into_inner().unwrap().1
}

struct Paginator<
	Manager,
	F: Fn(u32) -> Result<steamworks::UserListQuery<Manager>, steamworks::CreateQueryError>,
> {
	state: Arc<Mutex<PaginatorState>>,
	query_gen: F,
	_asdf: PhantomData<Manager>,
}
impl<
		Manager,
		F: Fn(u32) -> Result<steamworks::UserListQuery<Manager>, steamworks::CreateQueryError>,
	> Paginator<Manager, F>
{
	pub fn new(query_gen: F) -> Self {
		Self {
			state: Arc::new(Mutex::new(PaginatorState::new())),
			query_gen,
			_asdf: Default::default(),
		}
	}

	/// Checks if all the queries are finished yet, scheduling more work if needed.
	/// Call this until it returns true, running the steam callbacks in the meantime.
	pub fn done(&self) -> bool {
		let mut state = self.state.lock().unwrap();
		if state.current_page.is_none() || state.err.is_some() {
			return true;
		}

		if !state.query_pending {
			return false;
		}

		let state_rc2 = Arc::clone(&self.state);
		state.query_pending = false;
		let query = match (self.query_gen)(state.current_page.unwrap()) {
			Ok(q) => q,
			Err(err) => {
				state.err = Some(format!("{}", err));
				return true;
			}
		};

		query.fetch(move |res| {
			let mut state = state_rc2.lock().unwrap();
			let res = match res {
				Ok(res) => res,
				Err(err) => {
					state.err = Some(format!("{}", err));
					return;
				}
			};

			if res.returned_results() == 0 {
				state.current_page = None;
				return;
			}

			state.results.extend(res.iter());
			state.current_page = state.current_page.map(|i| i + 1);
			state.query_pending = true;
		});
		return false;
	}

	/// Gets the paginated results or error
	pub fn result(self) -> Result<Vec<steamworks::QueryResult>, String> {
		let state = Arc::try_unwrap(self.state)
			.map_err(|_| ())
			.expect("result called when not done")
			.into_inner()
			.unwrap();
		match state.err {
			None => Ok(state.results),
			Some(err) => Err(err),
		}
	}
}

struct PaginatorState {
	current_page: Option<u32>,
	query_pending: bool,
	results: Vec<steamworks::QueryResult>,
	err: Option<String>,
}
impl PaginatorState {
	fn new() -> Self {
		Self {
			current_page: Some(1),
			query_pending: true,
			results: vec![],
			err: None,
		}
	}
}
