extern crate ansi_term;
#[macro_use]
extern crate serde;
extern crate serde_json;
extern crate steamworks;
extern crate structopt;

mod local_collection;
mod subscriptions;

use ansi_term::Color;
use std::{
	collections::HashSet,
	env,
	fs::File,
	io::{
		self,
		BufRead,
		BufReader,
		BufWriter,
		Write,
	},
	path::PathBuf,
};
use structopt::StructOpt;

/// Tool for managing Steam workshop subscriptions in bulk
#[derive(Debug, StructOpt)]
#[structopt(name = "steam-workshop-subscription-manager")]
struct Main {
	#[structopt(subcommand)]
	subcommand: SubCommand,
}

#[derive(Debug, StructOpt)]
enum SubCommand {
	/// Exports a list of all currently subscribed workshop items for an app
	#[structopt(name = "export-subscribed", rename_all = "kebab-case")]
	ExportSubscribed {
		/// App ID of the game/app to export from
		app_id: u32,
		/// Destination JSON file
		out_file: PathBuf,
	},
	/// Imports a list of workshop items, subscribing to them
	#[structopt(name = "import", rename_all = "kebab-case")]
	Import {
		/// JSON file to import from
		in_file: PathBuf,
		/// Unsubscribe from items not in the imported lists, so that the currently subscribed
		/// items mirrors the imported list exactly.
		#[structopt(long = "uninstall-others")]
		uninstall_others: bool,
	},
	/// Unsubscribe from all workshop items for an app
	#[structopt(name = "unsubscribe-all", rename_all = "kebab-case")]
	UnsubscribeAll {
		/// App ID of the game/app to export from
		app_id: u32,
	},
	/// Adds items from a list to a Steam workshop collection
	#[structopt(name = "add-to-collection", rename_all = "kebab-case")]
	AddToCollection {
		/// ID of the collection
		collection_id: u64,
		/// JSON file to import from
		in_file: PathBuf,
	},
}

fn create_steam(
	app_id: u32,
) -> Result<
	(
		steamworks::Client<steamworks::ClientManager>,
		steamworks::SingleClient<steamworks::ClientManager>,
	),
	String,
> {
	env::set_var("SteamAppId", format!("{}", app_id));

	let (client, single) =
		steamworks::Client::init().map_err(|e| format!("Could not initialize steam: {}", e))?;

	let utils = client.utils();
	utils.set_warning_callback(|severity, msg| {
		eprintln!("{}: {:?}", if severity >= 1 { "WARN" } else { "INFO" }, msg);
	});

	Ok((client, single))
}

fn update_subscriptions(
	client: &steamworks::Client,
	single: &steamworks::SingleClient<steamworks::ClientManager>,
	mut to_add: Vec<local_collection::Item>,
	mut to_remove: Vec<local_collection::Item>,
) -> Result<bool, String> {
	to_add.sort_by(|a, b| a.name.cmp(&b.name));
	to_remove.sort_by(|a, b| a.name.cmp(&b.name));

	if to_add.is_empty() && to_remove.is_empty() {
		println!("Nothing to do");
		return Ok(false);
	}

	println!("Pending changes:");
	for add in to_add.iter() {
		println!(
			"{} {} ({})",
			Color::Green.paint("+"),
			add.name,
			add.published_file_id
		);
	}
	for rem in to_remove.iter() {
		println!(
			"{} {} ({})",
			Color::Red.paint("-"),
			rem.name,
			rem.published_file_id
		);
	}

	{
		print!("Continue (y/n)?");
		io::stdout()
			.lock()
			.flush()
			.map_err(|e| format!("Could not flush stdout: {}", e))?;
	}
	let stdin = io::stdin();
	match stdin.lock().lines().next() {
		Some(Ok(ref line)) if line.trim().eq_ignore_ascii_case("y") => {}
		Some(Ok(ref _line)) => {
			return Ok(false);
		}
		Some(Err(err)) => {
			return Err(format!("Could not read stdin: {}", err));
		}
		None => {
			return Ok(false);
		}
	};

	for add in to_add.iter() {
		println!("Subscribing to {} ({})", add.name, add.published_file_id);
		subscriptions::subscribe(
			client,
			single,
			steamworks::PublishedFileId(add.published_file_id),
		)
		.map_err(|e| {
			format!(
				"Could not subscribe to {} ({}): {}",
				add.name, add.published_file_id, e
			)
		})?;
	}
	for rem in to_remove.iter() {
		println!(
			"Unsubscribing from {} ({})",
			rem.name, rem.published_file_id
		);
		subscriptions::unsubscribe(
			client,
			single,
			steamworks::PublishedFileId(rem.published_file_id),
		)
		.map_err(|e| {
			format!(
				"Could not unsubscribe from {} ({}): {}",
				rem.name, rem.published_file_id, e
			)
		})?;
	}
	Ok(true)
}

fn main() {
	match real_main() {
		Ok(()) => {}
		Err(err) => {
			eprintln!("{}", err);
			::std::process::exit(1);
		}
	}
}

fn real_main() -> Result<(), String> {
	let opts = Main::from_args();

	match opts.subcommand {
		SubCommand::ExportSubscribed { app_id, out_file } => {
			let (client, single) = create_steam(app_id).map_err(|msg| {
				format!("{}\nEnsure that steam is running and that the app id corresponds to a game you own", msg)
			})?;
			let subscriptions = subscriptions::load_subscriptions(&client, &single)?;

			let collection = local_collection::Collection {
				app_id,
				items: subscriptions
					.iter()
					.map(|res| local_collection::Item {
						published_file_id: res.published_file_id.0,
						name: res.title.clone(),
					})
					.collect(),
			};

			let out_file = BufWriter::new(
				File::create(&out_file).map_err(|e| format!("Could not create out file: {}", e))?,
			);
			serde_json::to_writer_pretty(out_file, &collection)
				.map_err(|e| format!("Could not write output file: {}", e))?;

			println!("Wrote {} subscribed workshop items", subscriptions.len());
		}
		SubCommand::Import {
			in_file,
			uninstall_others,
		} => {
			let in_file = BufReader::new(
				File::open(&in_file).map_err(|e| format!("Could not open input file: {}", e))?,
			);

			let collection: local_collection::Collection = serde_json::from_reader(in_file)
				.map_err(|e| format!("Could not read input file: {}", e))?;

			let (client, single) = create_steam(collection.app_id).map_err(|msg| {
				format!("{}\nEnsure that steam is running and that the app id corresponds to a game you own", msg)
			})?;
			let subscriptions = subscriptions::load_subscriptions(&client, &single)?;

			let currently_subscribed_set = subscriptions
				.iter()
				.map(|res| local_collection::Item {
					published_file_id: res.published_file_id.0,
					name: res.title.clone(),
				})
				.collect::<HashSet<_>>();
			let collection_set = collection.items.iter().cloned().collect::<HashSet<_>>();
			let empty_set = HashSet::new();

			let new_items = collection_set.difference(&currently_subscribed_set);
			let delete_items = if uninstall_others {
				currently_subscribed_set.difference(&collection_set)
			} else {
				empty_set.difference(&empty_set)
			};

			update_subscriptions(
				&client,
				&single,
				new_items.cloned().collect(),
				delete_items.cloned().collect(),
			)?;
		}
		SubCommand::UnsubscribeAll { app_id } => {
			let (client, single) = create_steam(app_id).map_err(|msg| {
				format!("{}\nEnsure that steam is running and that the app id corresponds to a game you own", msg)
			})?;
			let subscriptions = subscriptions::load_subscriptions(&client, &single)?;

			update_subscriptions(
				&client,
				&single,
				vec![],
				subscriptions
					.iter()
					.map(|res| local_collection::Item {
						published_file_id: res.published_file_id.0,
						name: res.title.clone(),
					})
					.collect(),
			)?;
		}
		SubCommand::AddToCollection {
			collection_id,
			in_file,
		} => {
			let in_file = BufReader::new(
				File::open(&in_file).map_err(|e| format!("Could not open input file: {}", e))?,
			);
			let local_collection: local_collection::Collection =
				serde_json::from_reader(in_file)
					.map_err(|e| format!("Could not read input file: {}", e))?;

			let (client, single) = create_steam(local_collection.app_id).map_err(|msg| {
				format!("{}\nEnsure that steam is running and that the app id corresponds to a game you own", msg)
			})?;

			for item in local_collection.items.iter() {
				let res = subscriptions::add_to_collection(
					&client,
					&single,
					steamworks::PublishedFileId(collection_id),
					steamworks::PublishedFileId(item.published_file_id),
				);

				if let Err(err) = res {
					eprintln!("Could not add {}: {}", item.name, err);
				}
			}
		}
	};

	Ok(())
}
